<?php
add_action('init', 'register_product_category_taxonomy');

function register_product_category_taxonomy(){

	$labels = array(
		'name'          => _x( ' Products Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Product Category', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Products Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Products Categories', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Product Category', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Product Category', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Product Category', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Product Category', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('product-category','products', $args);
}
<?php
add_action('genesis_after_header' ,'wst_display_slider');
function wst_display_slider(){
	genesis_widget_area( 'slider');

}
add_action('genesis_after_header','wst_display_intro', 12);
function wst_display_intro(){ ?>
	<div class="archive-intro wrap">
		<h3 class="section-title">FILM-INSPIRED PRESETS</h3>
		<p>Handcrafted to emulate the subtle qualities of the world's finest <br/> films, and fit into any edition
			workflow.</p>
	</div>
<?php }
add_action('genesis_after_header', 'genesis_do_subnav',11);
remove_action('genesis_entry_header','genesis_do_post_title');
remove_action('genesis_entry_content','genesis_do_post_content');
add_action('genesis_entry_content', 'wst_display_product_grid');
function wst_display_product_grid(){
	get_template_part( 'views/products', 'grid' );
}



genesis();
<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
$thumb_url = $thumb_url_array[0];
?>
	<div class="product" >
		<div class="product-tag"><?php the_field('wst_tag');?></div>
		<a href="<?php the_permalink();?>">
			<div class="product-image"
			     style=" background: url(<?php echo $thumb_url; ?>);
				     -webkit-background-size: cover;
				     background-size: cover;
				     height: 250px;">
				<h2 class="product-title"><?php the_title(); ?></h2>
				<div class="product-spacer"></div>
				<h6 class="product-subtitle"><?php the_field( 'wst_subtitle' ); ?></h6>
			</div>
		</a>
		<div class="product-price"><?php the_field('wst_price');?></div>
		<h5 class="title-small"><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
		<h6 class="subtitle-small"><?php the_field('wst_subtitle');?></h6>
	</div>



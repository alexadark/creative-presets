<?php
$image = get_field('wst_second_image');
$url = $image['url'];
$alt = $image['alt'];
?>
<div class="section-four">    
    <div class="wrap">
        <div class="left-content">
            <?php the_field('wst_second_content'); ?>
        </div>
        <?php if (!empty($image)): ?>
            <img src="<?php echo $url; ?>"
                 alt="<?php echo $alt; ?>">
             <?php endif; ?>
    </div>
</div>